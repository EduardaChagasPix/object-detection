import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

def get_model_pretrained(num_classes):
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained = True, 
                                                                 min_size = 480, max_size = 640, 
                                                                 #rpn_pre_nms_top_n_train = 100, 
                                                                 rpn_pre_nms_top_n_test = 30,
                                                                 rpn_post_nms_top_n_test = 20)
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
    return model