import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset

import json
from PIL import ImageDraw
import albumentations as albu
import cv2
from skimage import measure
import pathlib as pl


def add_json_if_valid(in_folder, out_list):
    
    in_list = in_folder.glob('*.json')
    
    for json_file in in_list:
        
        gt_json = json.load(open(json_file, 'r'))
        valid = False
        
        for shape in gt_json['shapes']:
            
            if shape['shape_type'] == 'rectangle':
                valid = True
                break
        
        if valid:
            out_list.append(json_file)
            
def open_resize_mask(img_path, resolution, green_range):
    
    img = cv2.imread(str(img_path), cv2.IMREAD_COLOR)
    img = cv2.resize(img, resolution)
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    
    grenness = img[..., 1].astype(np.float32) / 255
    
    mean_grenn = np.mean(img_hsv[:50, :, 0]) * 0.5 + np.mean(img_hsv[-50:, :, 0]) * 0.5
    
    grenness = 1 - np.abs(img_hsv[..., 0].astype(np.float32) - mean_grenn) / (255 - mean_grenn)
    staturation = 1 - np.abs(img_hsv[..., 2].astype(np.float32) - 130) / 130
    comb = grenness * 0.75 + staturation * 0.25
    
    ferramenta = (grenness <= 0.95) & (staturation < 0.9)
    ferramenta = comb < 0.9
    ferramenta = grenness <= np.random.uniform(green_range[0], green_range[1])
    
    binary = measure.label(ferramenta, background = 0)
    coords = None
    for region in measure.regionprops(binary, coordinates='rc'):
        if region.area < 100:
            coords = region.coords
            ferramenta[coords[:, 0], coords[:, 1]] = 0
    
    
    background = np.logical_not(ferramenta)
    
    return img, ferramenta.astype(np.float32), background.astype(np.float32)

def get_bounding_box_from_seg(binary, width, height):
    
    region = np.where(binary)
    percentile = 0.0
    
    right = np.percentile(region[1], 100 - percentile)
    left = np.percentile(region[1], percentile)
    top = np.percentile(region[0], percentile)
    bottom = np.percentile(region[0], 100 - percentile)
    
    top = np.clip(top, 0, height - 1)
    bottom = np.clip(bottom, 0, height - 1)
    left = np.clip(left, 0, width - 1)
    right = np.clip(right, 0, width - 1)
    
    
    box_width, box_height = right - left, bottom - top
    
    return left, right, top, bottom, box_width, box_height
    


class LabelMeRectLoader(Dataset):
    """Segmentation dataset loader."""

    def __init__(self, datasets, resolution, class_name_to_idx, augmentation=False):
        """
        Este loader carrega bounding boxes. As coordenadas sao carregadas e sao
        utilizadas para "pintar"/"preencher" o retangulo na imagem, como se
        fosse uma tarefa de segmentacao. Com isso é feito o augmentation
        e depois, baseando-se no resultado de augmentation, as coordenadas
        do retangulo são (re)calculadas. Essa ideia permite fazer augmentations
        que nao funcionam com bounding boxes.
        
        Args:
            root (str): Path to folder that contains the annotations.
            resolution (tuple): (width, height) of the image.
            augmentation (bool): Do dataset augmentation (crete artificial variance) ?
        """

        self.gt_file_list = []

        for dataset in datasets:
            imgs = dataset['path'].glob('*.JPG')
            
            for img in imgs:
                
                item = {'img_path': img}
                item.update(dataset)
                self.gt_file_list.append(item)
            
        self.total_samples = len(self.gt_file_list)
        print('Total samples: %d' % self.total_samples)
        
        self.random_image_list = list(pl.Path('/home/caiom/Downloads/val2017').glob('*.jpg'))
        self.number_random_images = len(self.random_image_list)

        self.augmentation = augmentation
        self.resolution = resolution
        self.class_name_to_idx = class_name_to_idx

        # Mean and std are needed because we start from a pre trained net
        self.mean = [0.485, 0.456, 0.406]
        self.std = [0.229, 0.224, 0.225]
        
        self.aug_train = albu.Compose([
            albu.OneOf([
                    albu.IAAAdditiveGaussianNoise(p=1),
                    albu.GaussNoise(p=1),
                ], p=0.25),
            albu.OneOf([
                    albu.ElasticTransform(p=1, alpha=2000, sigma = 50, alpha_affine = 1, border_mode = cv2.BORDER_CONSTANT,mask_value = 0),
                    albu.OpticalDistortion(p=1, border_mode = cv2.BORDER_CONSTANT, mask_value = 0),
                ], p=0.33),
            albu.OneOf([
                albu.RandomBrightnessContrast(brightness_limit = 0.1, contrast_limit=0.1, p=1),    
                albu.RandomGamma(p=1),    
                ], p=0.5),
            albu.OneOf([
                albu.MotionBlur(p=1),
                albu.MedianBlur(blur_limit=3, p=1),
                albu.Blur(p=1)
                ], p = 0.1),
#            albu.HueSaturationValue(hue_shift_limit=10, sat_shift_limit=20, val_shift_limit=20, p=0.25),
            albu.HueSaturationValue(p=0.25),
            albu.ShiftScaleRotate(p=0.75, scale_limit=0.3, shift_limit=0.3, border_mode = cv2.BORDER_CONSTANT, rotate_limit=360, mask_value=0),
            albu.ToGray(p=0.2)
        ], p=1)

    def __len__(self):
        return self.total_samples

    def __getitem__(self, idx):
        
        if self.augmentation:
            item = self.gt_file_list[idx]
            img_path = item['img_path']
            fc = self.class_name_to_idx[item['name']]
            
            if self.augmentation:
                num_add_tools = np.random.randint(0, 4)
            else:
                num_add_tools = 0
            # Abre Json
            img, ferramenta, background = open_resize_mask(img_path, self.resolution, item['green_range'])
            
            add_tool_info = []
            for num_add_tool in range(num_add_tools):
                
                item_number = np.random.randint(0, self.total_samples)
                img2, ferramenta2, background2 = open_resize_mask(self.gt_file_list[item_number]['img_path'], self.resolution, self.gt_file_list[item_number]['green_range'])
                sc = self.class_name_to_idx[self.gt_file_list[item_number]['name']]
                add_tool_info.append({'img': img2, 'mask': ferramenta2, 'class': sc})
            
            
            if self.augmentation:
                ret = self.aug_train(image=img, mask=ferramenta)
                img, ferramenta = ret['image'], ret['mask'].astype(np.bool)
                
                for add_tool in add_tool_info:
                    ret = self.aug_train(image=add_tool['img'], mask=add_tool['mask'])
                    add_tool['img'], add_tool['mask'] = ret['image'], ret['mask'].astype(np.bool)
                    add_tool['bb'] = get_bounding_box_from_seg(add_tool['mask'], *self.resolution)
            
            
            bb1 = get_bounding_box_from_seg(ferramenta, *self.resolution)
            
            ferramentas = ferramenta
            
            for add_tool in add_tool_info:
                img[add_tool['mask']] = add_tool['img'][add_tool['mask']]
                ferramentas |= add_tool['mask']
                
    
            background_total = np.logical_not(ferramentas)
            
            #Não entendi - adiciona um fundo aleatório?
            if np.random.rand() < 0.5 and self.augmentation:
                img_number = np.random.randint(0, self.number_random_images)
                random_image = cv2.imread(str(self.random_image_list[img_number]), cv2.IMREAD_COLOR)
                random_image = cv2.cvtColor(random_image, cv2.COLOR_BGR2RGB)
                random_img_np = cv2.resize(random_image, self.resolution)
                img[background_total] = random_img_np[background_total]
    
            boxes = []
            labels = []
            labels.append(fc)
            boxes.append([bb1[0], bb1[2], bb1[1], bb1[3]])
            for add_tool in add_tool_info:
                boxes.append([add_tool['bb'][0], add_tool['bb'][2], add_tool['bb'][1], add_tool['bb'][3]])
                labels.append(add_tool['class'])
                
        else:
            
            img = np.load('data/%d_img.npy' % idx)
            with open('data/%d_data.json' % idx, 'r') as f:
                json_save = json.load(f)
                
            boxes = json_save['boxes']
            labels = json_save['labels']
            
#        json_save = {
#                'boxes': boxes,
#                'labels': labels
#        }
#        
#        import json
#        with open('data/%d_data.json' % idx, 'w') as f:
#            json.dump(json_save, f)
#        
#        np.save('data/%d_img' % idx, img)

        iscrowd = torch.tensor([0]*len(boxes), dtype=torch.int64)
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        # there is only one class

        labels = torch.as_tensor(labels, dtype=torch.int64)
        image_id = torch.tensor([idx])
        

        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
    
        target = {
          "boxes": boxes,
          "labels": labels,
          "image_id": image_id,
          "area": area,
          "iscrowd": iscrowd,
        }
        

        img_pt = img.astype(np.float32) / 255.0
        
        # There is normalization in the network
        
#        for i in range(3):
#            img_pt[..., i] -= self.mean[i]
#            img_pt[..., i] /= self.std[i]
            
        img_pt = img_pt.transpose(2,0,1)
            
        img_pt = torch.from_numpy(img_pt)
        return img_pt, target, img
