import torch
import pathlib as pl

# ---------------------------------------------------------------------------
DATASETS = [
            {'name': 'Carraca', 'path': pl.Path('Fotos/29.01 - CARRACA 1_ RESERVIBLE SIN MANGO'), 'green_range': (0.984, 0.987)},
            {'name': 'Alicate', 'path': pl.Path('Fotos/20.01 - ALICATES CORTE PLANO'), 'green_range': (0.97, 0.985)},
            {'name': 'Maza', 'path': pl.Path('Fotos/30.01 - MAZA ACERO 2 KG'), 'green_range': (0.97, 0.98)},
            {'name': 'Mordaza', 'path': pl.Path('Fotos/20.01 - MORDAZA UNIVERSAL'), 'green_range': (0.97, 0.985)}
            ]

class_name_to_idx = {'Carraca': 1,
                    'Alicate': 2,
                    'Maza': 3,
                    'Mordaza': 4}

class_idx_to_name = {1: 'Carraca',
                     2: 'Alicate',
                     3: 'Maza',
                     4: 'Mordaza'}
NUM_CLASSES = len(class_name_to_idx) + 1


SEED = 42
SCALE = 1
BATCH_SIZE = 4
NUM_WORKERS = 8
RESOLUTION = (640, 480)
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
#device = torch.device('cpu')

NUM_EPOCHS = 5
FREQ_EPOCHS = 1