import os
import cv2
import torch
import numpy as np
import pathlib as pl
import matplotlib.pyplot as plt
import albumentations as transforms

from PIL import Image
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import DataLoader, Dataset
from .mask import open_resize_mask
from .boundingBoxes import get_bounding_box_from_seg
from .engine import collate_fn
from .configs import *

def GetDataset():
    image_list = []
    data = []
    labels = []
    for dataset in DATASETS:
        imgs = dataset['path'].glob('*.JPG')
                
        for img in imgs:  
            item = {'img_path': img}
            img_path = item['img_path'] 
            image = cv2.imread(str(img_path), cv2.IMREAD_COLOR)
            image = cv2.resize(image, RESOLUTION)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) 
            label = class_name_to_idx[dataset['name']] 
            
            labels.append(label)
            data.append(image)
            item.update(dataset)
            image_list.append(item)
    return(image_list, data, labels)

def GetImageList():
    image_list = []
    for dataset in DATASETS:
        imgs = dataset['path'].glob('*.JPG')
                
        for img in imgs:  
            item = {'img_path': img}
            img_path = item['img_path'] 
            image = cv2.imread(str(img_path), cv2.IMREAD_COLOR)
            image = cv2.resize(image, RESOLUTION)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) 
            item.update(dataset)
            image_list.append(item)
    return(image_list)

class GetDataLoader(Dataset):

    def __init__(self, image_list, make_transforms = False):
        """
        Este loader carrega as imagens usadas no trabalho de classificação
        
        Args:
            datasets (str): Dicionário contendo: name, path e green_range.
            resolution (tuple): (width, height) of the image.
            class_name_to_idx: Dicionário com as categorias encontradas no dataset
            augmentation (bool): Do dataset augmentation (crete artificial variance) 
        """

        # Parameters   
        self.image_list = image_list
        self.resolution = RESOLUTION
        self.class_name_to_idx = class_name_to_idx
        self.transforms = transforms.Compose([
                            transforms.OneOf([transforms.IAAAdditiveGaussianNoise(p = 1), 
                                              transforms.GaussNoise(p = 1)], p = 0.25),
                            transforms.OneOf([transforms.ElasticTransform(p = 1, alpha = 2000, sigma = 50, alpha_affine = 1, 
                                                              border_mode = cv2.BORDER_CONSTANT,mask_value = 0),
                                            transforms.OpticalDistortion(p = 1, border_mode = cv2.BORDER_CONSTANT, mask_value = 0)], p = 0.33),
                            transforms.OneOf([transforms.RandomBrightnessContrast(brightness_limit = 0.1, contrast_limit = 0.1, p = 1),    
                                             transforms.RandomGamma(p = 1)], p=0.5),
                            transforms.OneOf([transforms.MotionBlur(p = 1), 
                                            transforms.MedianBlur(blur_limit = 3, p = 1),
                                            transforms.Blur(p = 1)], p = 0.1),
                                            transforms.HueSaturationValue(p = 0.25),
                                            transforms.ShiftScaleRotate(p = 0.75, scale_limit = 0.3, shift_limit = 0.3, 
                                                              border_mode = cv2.BORDER_CONSTANT, rotate_limit = 360, mask_value = 0),
                                            transforms.ToGray(p = 0.2)], p = 1)
        self.make_transforms = make_transforms
        self.total_samples = len(self.image_list)    
        self.random_image_list = list(pl.Path('Fotos/Random-Images').glob('*.jpeg'))
        self.number_random_images = len(self.random_image_list) 
        
        #print('Total samples: %d' % self.total_samples)      
        
        # Mean and std are needed because we start from a pre trained net
        self.mean = [0.485, 0.456, 0.406]
        self.std = [0.229, 0.224, 0.225]
                
    def __len__(self):
        return self.total_samples
    
    def __getitem__(self, i):
        
        # Define imagens e classes
        item = self.image_list[i]
        img_path = item['img_path']
        img_label = self.class_name_to_idx[item['name']]
            
        # Segmenta a imagem 
        img, tool_mask, background = open_resize_mask(img_path, self.resolution, item['green_range'])
        
        if self.make_transforms:
                            
            # Aplica augmentation
            ret = self.transforms(image = img, mask = tool_mask)
            img, tool_mask = ret['image'], ret['mask'].astype(np.bool)
            img_box = get_bounding_box_from_seg(tool_mask, *self.resolution)
            
            
            # Pega ferramentas de modo aleatório para adicionar na imagem
            num_add_tools = np.random.randint(0, 4)
            add_tool_info = []
            for num_add_tool in range(num_add_tools):                
                item_index = np.random.randint(0, self.total_samples) 
                img_2, ferramenta_2, background_2 = open_resize_mask(self.image_list[item_index]['img_path'], 
                                                                     self.resolution, 
                                                                     self.image_list[item_index]['green_range'])
                sc = self.class_name_to_idx[self.image_list[item_index]['name']]
                add_tool_info.append({'img': img_2, 'mask': ferramenta_2, 'class': sc})                
            
            # Faz augmentation e calcula o bouding box
            for add_tool in add_tool_info:
                ret = self.transforms(image = add_tool['img'], mask = add_tool['mask'])
                add_tool['img'], add_tool['mask'] = ret['image'], ret['mask'].astype(np.bool)
                add_tool['bb'] = get_bounding_box_from_seg(add_tool['mask'], *self.resolution)
            
            ferramentas = tool_mask
            for add_tool in add_tool_info:
                img[add_tool['mask']] = add_tool['img'][add_tool['mask']]
                ferramentas |= add_tool['mask']
                                             
            # Adiciona novo background
            background_total = np.logical_not(ferramentas)    
            if np.random.rand() < 0.5:
                index_img = np.random.randint(0, self.number_random_images)
                background_image = cv2.imread(str(self.random_image_list[index_img]))
                background_image = cv2.cvtColor(background_image, cv2.COLOR_BGR2RGB)
                background_image_np = cv2.resize(background_image, self.resolution)
                img[background_total] = background_image_np[background_total]
            else:
                background_image = cv2.imread('Fotos/White/white.png')
                background_image = cv2.cvtColor(background_image, cv2.COLOR_BGR2RGB)
                background_image_np = cv2.resize(background_image, self.resolution)
                img[background_total] = background_image_np[background_total]
            
            #Merge images, boxes and labels
            boxes = []
            labels = [0]
            labels.append(img_label)
            boxes.append([img_box[0], img_box[2], img_box[1], img_box[3]])
            for add_tool in add_tool_info:
                boxes.append([add_tool['bb'][0], add_tool['bb'][2], add_tool['bb'][1], add_tool['bb'][3]])
                labels.append(add_tool['class'])
        else:                            
            # Segmenta a imagem 
            img, tool_mask, background = open_resize_mask(img_path, self.resolution, item['green_range'])
            img_box = get_bounding_box_from_seg(tool_mask, *self.resolution)
            
            # save
            boxes = []
            #labels = []
            labels = []
            labels.append(img_label)
            boxes.append([img_box[0], img_box[2], img_box[1], img_box[3]])
            
        iscrowd = torch.tensor([0]*len(boxes), dtype=torch.int64)
        boxes = torch.as_tensor(boxes, dtype=torch.float32)        
        labels = torch.as_tensor(labels, dtype=torch.int64)
        image_id = torch.tensor([i])        
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])  
        
        target = {
          "boxes": boxes,
          "labels": labels,
          "image_id": image_id,
          "area": area,
          "iscrowd": iscrowd,
        }
        
        # Normalizando a imagem
        img_pt = img.astype(np.float32) / 255.0
        img_pt = img_pt.transpose(2,0,1)        
        img_pt = torch.from_numpy(img_pt)
        
        return img_pt, target, img  

def get_loader(data_type):

    image_list = GetImageList()

    if(data_type == "TRAIN"):
        dataset = GetDataLoader(image_list, True)
        indices = torch.randperm(len(dataset)).tolist()
        dataset = torch.utils.data.Subset(dataset, indices[:-30])
        data_loader = torch.utils.data.DataLoader(dataset, 
                                                  batch_size = BATCH_SIZE, 
                                                  shuffle = True, 
                                                  num_workers = NUM_WORKERS,
                                                  collate_fn = collate_fn,
                                                  worker_init_fn = np.random.seed(torch.initial_seed() % 2 ** 32))
    if(data_type == "TEST"):
        dataset = GetDataLoader(image_list, False)
        indices = torch.randperm(len(dataset)).tolist()
        dataset = torch.utils.data.Subset(dataset, indices[-82:])
        data_loader = torch.utils.data.DataLoader(dataset, 
                                                  batch_size = BATCH_SIZE, 
                                                  shuffle = False, 
                                                  num_workers = NUM_WORKERS,
                                                  collate_fn = collate_fn, 
                                                  worker_init_fn = np.random.seed(torch.initial_seed() % 2 ** 32))
    return(data_loader)
