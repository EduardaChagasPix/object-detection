import numpy as np

def get_bounding_box_from_seg(binary, width, height):
    
    region = np.where(binary)
    percentile = 0.0
    
    right = np.percentile(region[1], 100 - percentile)
    left = np.percentile(region[1], percentile)
    top = np.percentile(region[0], percentile)
    bottom = np.percentile(region[0], 100 - percentile)
    
    top = np.clip(top, 0, height - 1)
    bottom = np.clip(bottom, 0, height - 1)
    left = np.clip(left, 0, width - 1)
    right = np.clip(right, 0, width - 1)
    
    
    box_width, box_height = right - left, bottom - top
    
    return left, right, top, bottom, box_width, box_height