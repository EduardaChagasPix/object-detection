import cv2
import numpy as np
from skimage import measure

def open_resize_mask(img_path, resolution, green_range):
    
    img = cv2.imread(str(img_path), cv2.IMREAD_COLOR)
    img = cv2.resize(img, resolution)
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    
    grenness = img[..., 1].astype(np.float32) / 255
    
    mean_grenn = np.mean(img_hsv[:50, :, 0]) * 0.5 + np.mean(img_hsv[-50:, :, 0]) * 0.5
    
    grenness = 1 - np.abs(img_hsv[..., 0].astype(np.float32) - mean_grenn) / (255 - mean_grenn)
    staturation = 1 - np.abs(img_hsv[..., 2].astype(np.float32) - 130) / 130
    comb = grenness * 0.75 + staturation * 0.25
    
    ferramenta = (grenness <= 0.95) & (staturation < 0.9)
    ferramenta = comb < 0.9
    ferramenta = grenness <= np.random.uniform(green_range[0], green_range[1])
    
    binary = measure.label(ferramenta, background = 0)
    coords = None
    for region in measure.regionprops(binary):
        if region.area < 100:
            coords = region.coords
            ferramenta[coords[:, 0], coords[:, 1]] = 0
    
    
    background = np.logical_not(ferramenta)
    
    return img, ferramenta.astype(np.float32), background.astype(np.float32)
    
 
