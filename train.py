from lib.configs import *
from lib.datasetLoader import get_loader 
from lib.modelPretrained import get_model_pretrained
from lib.engine import train_one_epoch, evaluate

def main():
   # Load a model pre-trained on COCO
    model = get_model_pretrained(NUM_CLASSES)
    model.eval()
    model.to(DEVICE)

    # Construct an optimizer
    optimizer = torch.optim.SGD(model.parameters(), 
                                lr = 0.01, 
                                momentum = 0.9,
                                weight_decay = 0.0005)

    # Learning rate scheduler
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 
                                                              mode = 'max', 
                                                              patience = 10, 
                                                              verbose = True)

    train_loader = get_loader("TRAIN")
    test_loader = get_loader("TEST")

    best_eval_metric = 0
    for epoch in range(NUM_EPOCHS):
        train_one_epoch(model, optimizer, train_loader, DEVICE, epoch, print_freq = 10)
        evaluation_ret = evaluate(model, test_loader, DEVICE)
        print(evaluation_ret)
    
        #eval_metric = evaluation_ret.coco_eval['bbox'].stats[1] * 0.8 + evaluation_ret.coco_eval['bbox'].stats[2] * 0.2        
        #lr_scheduler.step(eval_metric)
    
        #if  epoch == (NUM_EPOCHS-1) or eval_metric > best_eval_metric:
        #    print('New best Model!')
        #    best_eval_metric = eval_metric
        #    save_on_master({
        #        'model': model.state_dict(),
        #        'optimizer': optimizer.state_dict(),
        #        'lr_scheduler': lr_scheduler.state_dict()},
        #        'model_best.pth')


if __name__ == "__main__":
    main()
