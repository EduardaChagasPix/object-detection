# Detecção de ferramentas com o uso de Deep Learning

### Esse repositório contém todos os dados e códigos utilizados para a implementação do sistema de detecção e reconhecimento de ferramentas para o projeto Termope

---

### Especificação do projeto

Este trabaho consiste de uma das etapas de digitalização da zona limpa da Termope. Atualmente a checagem de ferramentas é realizada de forma manual por um vigilante.
O colaborador deverá ir até o container de ferramentas para solicitar e receber as ferramentas que serão utilizadas neste dia. O ferramenteiro entregará ao colaborador 
e após esse momento, o vigilante faz uma checagem, onde identificará as ferramentas que irão acessar a área controlada da turbina. O presente algoritmo possui como 
objetivo reconhecer as 400 ferramentas presentes no container com > 80% de acurácia.

### Metodologia

Nossa proposta consiste na implementação dos seguintes passos:

1. Detecção da ferramenta nas imagens de teste/treinamento;	

2. Remoção da ferramenta da imagem;	

3. Geração de dataset sintético por meio da combinação de ferramentas e adição de fundos aleatórios;	

4. Treinamento de técnicas de redes neurais ;

### Dataset

- ~112 imagens de 4 ferramentas distintas;
- 8 vídeos de 4 ferramentas distintas;
- Imagens tiradas com câmera profissional e fundo verde.

![Tool images](https://bitbucket.org/EduardaChagasPix/object-detection/raw/12792c4d7a49297a7a9c9ad5892c166eee424683/Images/tools.png)

### Data augmentation

- Nenhuma anotação manual foi realizada;
- Além de combinar ferramentas, modificamos suas posições, tamanhos, iluminação, distorção e blur.

![Augmentation](https://bitbucket.org/EduardaChagasPix/object-detection/raw/12792c4d7a49297a7a9c9ad5892c166eee424683/Images/augmentation.png)


